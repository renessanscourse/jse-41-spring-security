package ru.ovechkin.tm.soap.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * http://localhost:8080/ws/TaskEndpoint?wsdl
 */
@Component
@WebService
public class TaskEndpoint {

    @Autowired
    private ITaskService taskService;

    @WebMethod
    public List<Task> findAllTask(@WebParam(name = "projectId") @Nullable String projectId) {
        return taskService.findAll(projectId, "ЗАГЛУШКА");
    }

    @WebMethod
    public void saveTask(@WebParam(name = "task") @Nullable Task task) {
        taskService.save(task, "ЗАГЛУШКА");
    }

    @WebMethod
    public void removeTaskById(@WebParam(name = "taskId") @Nullable String taskId) {
        taskService.removeById(taskId, "ЗАГЛУШКА");
    }

    @NotNull
    @WebMethod
    public Task findTaskById(@WebParam(name = "taskId") @Nullable String taskId) {
        return taskService.findById(taskId);
    }

    @WebMethod
    public void updateTaskById(
            @WebParam(name = "taskId") @Nullable String taskId,
            @WebParam(name = "projectId") @Nullable String projectId,
            @WebParam(name = "task") @Nullable Task task
    ) {
        taskService.updateById(taskId, task, "ЗАГЛУШКА");
    }

}