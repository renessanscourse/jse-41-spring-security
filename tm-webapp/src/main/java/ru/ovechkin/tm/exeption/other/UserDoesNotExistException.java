package ru.ovechkin.tm.exeption.other;

public class UserDoesNotExistException extends RuntimeException {

    public UserDoesNotExistException() {
        super("Error! This user does not exist in system...");
    }

}
