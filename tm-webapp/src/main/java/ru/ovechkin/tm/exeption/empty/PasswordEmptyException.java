package ru.ovechkin.tm.exeption.empty;

public class PasswordEmptyException extends RuntimeException {

    public PasswordEmptyException() {
        super("Error! Password is empty...");
    }

}