package ru.ovechkin.tm.entity;

public class Result {

    private Boolean success = true;

    private String message = "";

    public Result(Exception e) {
        this.success = false;
        this.message = e.getMessage();
    }

    public Result() {
    }

}