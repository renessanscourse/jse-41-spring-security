package ru.ovechkin.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.exeption.empty.IdEmptyException;
import ru.ovechkin.tm.exeption.empty.ProjectEmptyException;
import ru.ovechkin.tm.exeption.empty.ProjectIdEmptyException;
import ru.ovechkin.tm.exeption.empty.UserEmptyException;
import ru.ovechkin.tm.exeption.other.UserDoesNotExistException;
import ru.ovechkin.tm.exeption.unknown.ProjectUnknownException;
import ru.ovechkin.tm.exeption.user.AccessDeniedException;
import ru.ovechkin.tm.repository.ProjectRepository;
import ru.ovechkin.tm.repository.UserRepository;

import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<Project> findAllUserProjects(final String userName) {
        if (userName == null || userName.isEmpty()) throw new UsernameNotFoundException(userName);
        final User user = userRepository.findByLogin(userName);
        if (user == null) throw new UserDoesNotExistException();
        return projectRepository.findAllByUserId(user.getId());
    }

    @Override
    @Transactional
    public void save(@Nullable final Project project, @Nullable final String userName) {
        if (userName == null || userName.isEmpty()) throw new UsernameNotFoundException(userName);
        if (project == null) throw new ProjectUnknownException();
        final User user = userRepository.findByLogin(userName);
        if (user == null) throw new UserDoesNotExistException();
        project.setUser(user);
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String projectId, @Nullable final String userName) {
        if (userName == null || userName.isEmpty()) throw new UsernameNotFoundException(userName);
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final User user = userRepository.findByLogin(userName);
        if (user == null) throw new UserDoesNotExistException();
        @Nullable final Project project =
                projectRepository.findById(projectId).orElse(null);
        if (project == null) throw new ProjectUnknownException(projectId);
        if (!project.getUser().getId().equals(user.getId())) throw new AccessDeniedException();
        projectRepository.deleteById(projectId);
    }

    @Override
    public Project findById(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final Project project =
                projectRepository.findById(projectId).orElse(null);
        if (project == null) throw new ProjectUnknownException(projectId);
        return project;
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String id,
            @Nullable final Project project,
            @Nullable final String userName
    ) {
        if (project == null) throw new ProjectEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userName == null || userName.isEmpty()) throw new UsernameNotFoundException(userName);
        @Nullable final Project projectToUpdate =
                projectRepository.findById(id).orElse(null);
        if (projectToUpdate == null) throw new ProjectUnknownException(id);
        @Nullable final User user = userRepository.findByLogin(userName);
        if (user == null) throw new UserDoesNotExistException();
        if (!projectToUpdate.getUser().getId().equals(user.getId())) throw new AccessDeniedException();
        projectToUpdate.setName(project.getName());
        projectToUpdate.setDescription(project.getDescription());
        projectToUpdate.setUser(user);
        projectRepository.save(projectToUpdate);
    }

}