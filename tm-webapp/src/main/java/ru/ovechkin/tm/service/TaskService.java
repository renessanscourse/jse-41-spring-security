package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.exeption.empty.IdEmptyException;
import ru.ovechkin.tm.exeption.other.NotLoggedInException;
import ru.ovechkin.tm.exeption.other.UserDoesNotExistException;
import ru.ovechkin.tm.exeption.unknown.LoginUnknownException;
import ru.ovechkin.tm.exeption.unknown.TaskUnknownException;
import ru.ovechkin.tm.exeption.user.AccessDeniedException;
import ru.ovechkin.tm.repository.ProjectRepository;
import ru.ovechkin.tm.repository.TaskRepository;
import ru.ovechkin.tm.repository.UserRepository;

import java.util.List;

@Service
public class TaskService implements ITaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable final String projectId,
            @Nullable final String userName
    ) {
        @Nullable final User user = userRepository.findByLogin(userName);
        if (user == null) throw new UserDoesNotExistException();
        return taskRepository.findByProjectIdAndUserId(projectId, user.getId());
    }

    @Override
    @Transactional
    public void save(@Nullable final Task task, @Nullable final String userName) {
        if (userName == null || userName.isEmpty()) throw new UserDoesNotExistException();
        if (task == null) throw new TaskUnknownException();
        task.setUser(userRepository.findByLogin(userName));
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String taskId, @Nullable final String userName) {
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        if (userName == null || userName.isEmpty()) throw new NotLoggedInException();
        @Nullable final User user = userRepository.findByLogin(userName);
        if (user == null) throw new UserDoesNotExistException();
        @NotNull final Task task = findById(taskId);
        if (!task.getUser().getId().equals(user.getId())) throw new AccessDeniedException();
        taskRepository.delete(task);
    }

    @NotNull
    @Override
    public Task findById(@Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = taskRepository.findById(taskId).orElse(null);
        if (task == null) throw new TaskUnknownException();
        return task;
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String taskId,
            @Nullable final Task task,
            @Nullable final String userName
    ) {
        if (userName == null || userName.isEmpty()) throw new NotLoggedInException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        if (task == null) throw new TaskUnknownException();
        @Nullable final Task taskToUpdate = taskRepository.findById(taskId).orElse(null);
        if (taskToUpdate == null) throw new TaskUnknownException();
        @Nullable final User user = userRepository.findByLogin(userName);
        if (user == null) throw new UserDoesNotExistException();
        if (!taskToUpdate.getUser().getId().equals(user.getId())) throw new AccessDeniedException();
        taskToUpdate.setName(task.getName());
        taskToUpdate.setDescription(task.getDescription());
        taskRepository.save(taskToUpdate);
    }

}