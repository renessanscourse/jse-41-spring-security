package ru.ovechkin.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ovechkin.tm.entity.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    List<Project> findAllByUserId(final String userId);

    void deleteTaskById(final String taskId);

}