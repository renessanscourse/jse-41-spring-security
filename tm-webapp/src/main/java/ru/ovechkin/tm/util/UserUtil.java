package ru.ovechkin.tm.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import ru.ovechkin.tm.exeption.user.AccessDeniedException;

public class UserUtil {

    static public String userName() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException();
//        if (!(principal instanceof User)) throw new AccessDeniedException();
        final User user = (User) principal;
        return user.getUsername();
    }
}
