package ru.ovechkin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.entity.Project;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @GetMapping("/all")
    public String allProjects(
            Model model,
            @AuthenticationPrincipal User user
    ) {
        model.addAttribute("projects", projectService.findAllUserProjects(user.getUsername()));
        return "projects/all";
    }

    @GetMapping("/createForm")
    public String getCreateProjectForm(Model model) {
        model.addAttribute("project", new Project());
        return "projects/create";
    }

    @PostMapping("/create")
    public String create(
            @ModelAttribute("project") Project project,
            @AuthenticationPrincipal User user
    ) {
        projectService.save(project, user.getUsername());
        return "redirect:/projects/all";
    }

    @GetMapping("/remove")
    public String remove(
            @RequestParam("projectId") String projectId,
            @AuthenticationPrincipal User user
    ) {
        projectService.removeById(projectId, user.getUsername());
        return "redirect:/projects/all";
    }

    @GetMapping("/{id}")
    public String getEditForm(@PathVariable("id") final String projectId, Model model) {
        model.addAttribute("project", projectService.findById(projectId));
        return "/projects/edit";
    }

    @PostMapping("/edit")
    public String edit(
            @RequestParam("id") final String projectId,
            @ModelAttribute("project") final Project project,
            @AuthenticationPrincipal User user
    ) {
        projectService.updateById(projectId, project, user.getUsername());
        return "redirect:/projects/all";
    }


}