package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.ovechkin.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAllUserProjects(String userName);

    @Transactional
    void save(@Nullable Project project, @Nullable String userName);

    @Transactional
    void removeById(@Nullable String projectId, @Nullable final String userName);

    Project findById(@Nullable String projectId);

    @Transactional
    void updateById(@Nullable String id, @Nullable Project project, @Nullable final String userName);
}