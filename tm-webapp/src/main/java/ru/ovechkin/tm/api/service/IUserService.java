package ru.ovechkin.tm.api.service;

import ru.ovechkin.tm.entity.User;

public interface IUserService {

    User findByLogin(String login);

    void registry(User user);

}
